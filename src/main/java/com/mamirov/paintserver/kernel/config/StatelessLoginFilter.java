package com.mamirov.paintserver.kernel.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mamirov.paintserver.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class StatelessLoginFilter extends AbstractAuthenticationProcessingFilter {

    PaintLoginUserDetailService userDetailService;

    Logger logger = LoggerFactory.getLogger(StatelessLoginFilter.class.getName());

    protected StatelessLoginFilter(String url, PaintLoginUserDetailService userDetailService, AuthenticationManager authManager) {
        super(new AntPathRequestMatcher(url));
        this.userDetailService = userDetailService;
        setAuthenticationManager(authManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {
        if (SecurityContextHolder.getContext().getAuthentication() != null && SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            return SecurityContextHolder.getContext().getAuthentication();
        }
        final User user = new ObjectMapper().readValue(httpServletRequest.getInputStream(), User.class);
        String referer = httpServletRequest.getHeader("referer");
        if(referer != null && referer.contains("viewer_id=" + user.getVkId()) && referer.contains("access_token")) {
            if(userDetailService.loadUserByUsername(user.getLogin()) == null) {
                logger.info("User created: " + user);
                userDetailService.save(user);
            }
            final UsernamePasswordAuthenticationToken loginToken = new UsernamePasswordAuthenticationToken(
                    user.getLogin(), user.getVkId());

            return getAuthenticationManager().authenticate(loginToken);
        }
        return null;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        if (SecurityContextHolder.getContext().getAuthentication() != null && SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            return;
        }

        PaintUserDetails userDetails = userDetailService.loadUserByUsername(authResult.getName());

        SecurityContextHolder.getContext().setAuthentication(new UserAuthentication(userDetails.getUser()));
    }
}
