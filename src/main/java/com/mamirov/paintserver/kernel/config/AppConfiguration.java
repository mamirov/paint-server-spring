package com.mamirov.paintserver.kernel.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.sql.DataSource;

@Configuration
public class AppConfiguration {

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource primaryDataSource() {
        return DataSourceBuilder.create().type(HikariDataSource.class).build();
    }

    @Bean
    public WebMvcConfigurerAdapter forwardToIndex() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addResourceHandlers(ResourceHandlerRegistry registry) {
                registry.addResourceHandler("/vk/**").addResourceLocations("file:///C:\\Users\\Marat\\Documents\\paint-html-client\\");
                registry.addResourceHandler("/img/**").addResourceLocations("file:///C:\\drawme-png\\");
                registry.addResourceHandler("/resources/**").addResourceLocations("file:///C:\\Users\\Marat\\IdeaProjects\\paint-server\\src\\main\\resources\\web\\");
                super.addResourceHandlers(registry);
            }

            @Override
            public void addViewControllers(ViewControllerRegistry registry) {
                registry.addViewController("/").setViewName("forward:/resources/index.html");
                registry.addViewController("/vk").setViewName("forward:/vk/app/components/home/views/home.html");

            }
        };
    }
}
