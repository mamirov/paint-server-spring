package com.mamirov.paintserver.kernel.config;

import com.mamirov.paintserver.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.inject.Inject;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Inject
    private PaintLoginUserDetailService paintLoginUserDetailService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.headers().frameOptions().httpStrictTransportSecurity().disable();
        http.csrf().disable();
        http
                .exceptionHandling().and()
                .anonymous().and()
                .servletApi().and()
                .headers().cacheControl().and()
                .authorizeRequests()

                        //allow anonymous resource requests
                .antMatchers("/vk/**").permitAll()
                .antMatchers("/img/**").permitAll()
                .antMatchers("/resources/**").permitAll()

                        //allow anonymous POSTs to login
                .antMatchers(HttpMethod.POST, "/api/v1/login").permitAll()

                        //allow anonymous GETs to API
//                .antMatchers(HttpMethod.GET, "/api/**").permitAll()

                        //defined Admin only API area
                .antMatchers("/admin/**").hasAuthority(Role.ADMIN.toString())

                        //all other request need to be authenticated
                .anyRequest().hasAnyAuthority(Role.CLIENT.toString(), Role.ADMIN.toString()).and()
                .addFilterBefore(new StatelessLoginFilter("/api/v1/login", paintLoginUserDetailService, authenticationManager()), UsernamePasswordAuthenticationFilter.class);


    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(paintLoginUserDetailService).and();
    }

}
