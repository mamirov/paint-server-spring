package com.mamirov.paintserver.kernel.config;

import com.mamirov.paintserver.model.Role;
import com.mamirov.paintserver.model.User;
import com.mamirov.paintserver.service.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class PaintLoginUserDetailService implements UserDetailsService {

    @Inject
    private UserService userService;

    @Override
    public PaintUserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userService.findByLogin(login);
        if(null == user) {
            return null;
        }
        return new PaintUserDetails(user);
    }

    public User save(User user) {
        if(user.getLogin().equals("mamirof")) {
            user.setRole(Role.ADMIN);
        } else {
            user.setRole(Role.CLIENT);
        }
        return userService.save(user);
    }
}
