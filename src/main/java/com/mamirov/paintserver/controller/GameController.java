package com.mamirov.paintserver.controller;

import com.mamirov.paintserver.controller.res.UploadResult;
import com.mamirov.paintserver.exception.GameException;
import com.mamirov.paintserver.model.Picture;
import com.mamirov.paintserver.model.User;
import com.mamirov.paintserver.model.UserNotification;
import com.mamirov.paintserver.model.UserNotificationType;
import com.mamirov.paintserver.service.UserService;
import com.mamirov.paintserver.utils.PaintContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Date;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("/api/v1/game")
public class GameController {

    Logger logger = LoggerFactory.getLogger(GameController.class.getName());

    @Autowired
    private UserService userService;


    @ExceptionHandler
    void handleIllegalArgumentException(GameException e, HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.OK.value());
    }

    @RequestMapping(method = RequestMethod.GET)
    public User startGame () throws GameException {
        User currentUser = PaintContext.getCurrentUser();
        if(currentUser == null) {
            throw new GameException("Вы не авторизованы");
        }

        currentUser = userService.findByVkId(currentUser.getVkId());
        if(currentUser.getEnergy() < 5) {
            throw new GameException("Недостаточно энергии");
        }

        Random random = new Random();
        List<User> targetUsers = userService.findByVkIdNot(currentUser.getVkId());
        if(targetUsers.isEmpty()) {
            throw new GameException("Не найдено пользователей для игры :(");
        }
        int randomIndex = random.nextInt(targetUsers.size());
        return targetUsers.get(randomIndex);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/upload")
    public UploadResult uploadImg(@RequestParam("targetUser") String targetUserId, @RequestParam("file") MultipartFile file) throws GameException {
        User user = PaintContext.getCurrentUser();
        user = userService.findByVkId(user.getVkId());
        UploadResult uploadResult = new UploadResult();
        if(user.getEnergy() < 5) {
            uploadResult.setStatus("fail");
            uploadResult.setMessage("Недостаточно энергии");
            return uploadResult;
        }
        if (!file.isEmpty()) {
            try {
                String pathDir = "C:\\drawme-png\\";


                byte[] bytes = file.getBytes();
                String data = "";
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                BufferedOutputStream stream =
                        new BufferedOutputStream(baos);
                stream.write(bytes);
                stream.close();
                data = baos.toString();
                data = data.replace("data:image/png;base64,", "");

                byte[] bytearray = Base64.decode(data.getBytes());
                BufferedImage imag = ImageIO.read(new ByteArrayInputStream(bytearray));
                String link = user.getVkId() + new Date().getTime() + ".png";
                ImageIO.write(imag, "png", new File(pathDir + link));

                Picture picture = new Picture();
                User targetUser = userService.findByVkId(targetUserId);
                UserNotification userNotification = new UserNotification();
                userNotification.setType(UserNotificationType.DRAWED);
                userNotification.setTargetUser(targetUser);
                targetUser.getNotifications().add(userNotification);
                picture.setAuthor(user);
                picture.setTargetUser(targetUser);
                picture.setLink(link);

                if(user.getEnergy() == 30) {
                    Date date = new Date();
                    long updateDate = date.getTime() + 1000*60*5;
                    user.setEnergyUpdate(new Date(updateDate));
                }
                user.getDrawPictures().add(picture);
                user.setEnergy(user.getEnergy() - 5);
                user.setExperience(user.getExperience() + 10);
                userService.save(user);
                uploadResult.setStatus("success");
                uploadResult.setMessage("Рисунок успешно загружен!");
                return uploadResult;
            } catch (Exception e) {
                e.printStackTrace();
                logger.error("Error from uploading file", e);
                uploadResult.setStatus("fail");
                uploadResult.setMessage(e.getMessage());
                return uploadResult;
            }
        } else {
            uploadResult.setStatus("fail");
            uploadResult.setMessage("Рисунок пуст");
            return uploadResult;
        }
    }
}
