package com.mamirov.paintserver.controller;

import com.mamirov.paintserver.model.Picture;
import com.mamirov.paintserver.model.User;
import com.mamirov.paintserver.service.PictureService;
import com.mamirov.paintserver.service.UserService;
import com.mamirov.paintserver.utils.PaintContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    Logger logger = LoggerFactory.getLogger(UserController.class.getName());

    @Autowired
    private UserService userService;

    @Autowired
    private PictureService pictureService;

    @RequestMapping(method = RequestMethod.GET, value = "/info")
    public User getCurrentUser() {
        User user = PaintContext.getCurrentUser();
        return userService.findById(user.getId());
    }

    @RequestMapping(method = RequestMethod.GET, value = "/album")
    public List<Picture> getAlbum(){
        User user = PaintContext.getCurrentUser();
        return pictureService.findByAuthor(user);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/mypictures")
    public List<Picture> getMyPictures(){
        User user = PaintContext.getCurrentUser();
        user = userService.findById(user.getId());
        user.getNotifications().clear();
        userService.save(user);
        return pictureService.findByTargetUser(user);
    }
}
