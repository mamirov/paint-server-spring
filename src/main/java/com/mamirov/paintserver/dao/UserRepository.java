package com.mamirov.paintserver.dao;

import com.mamirov.paintserver.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {
    User findByLogin(String login);
    User findByVkId(String vkId);
    List<User> findByEnergyLessThan(Integer energy);
    List<User> findByVkIdNot(String vkId);
}
