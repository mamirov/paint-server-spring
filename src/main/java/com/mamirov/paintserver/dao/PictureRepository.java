package com.mamirov.paintserver.dao;

import com.mamirov.paintserver.model.Picture;
import com.mamirov.paintserver.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PictureRepository extends JpaRepository<Picture, Integer> {
    List<Picture> findByAuthor(User user);
    List<Picture> findByTargetUser(User targetUser);
}
