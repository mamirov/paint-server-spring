package com.mamirov.paintserver;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.File;
import java.io.IOException;

import static springfox.documentation.builders.PathSelectors.regex;

@SpringBootApplication
@EntityScan(basePackages = "com.mamirov.paintserver.model")
@EnableJpaRepositories(basePackages = "com.mamirov.paintserver.dao")
@ComponentScan(basePackages = "com.mamirov.paintserver")
@EnableScheduling
@EnableSwagger2
public class PaintApplication {

    public static void main(String [] args) {
        SpringApplication.run(PaintApplication.class, args);
    }

    @Bean
    public Docket newsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("api-help")
                .apiInfo(apiInfo())
                .select()
                .paths(regex("/api/v1/.*"))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Документация REST API")
                .description("Документация REST API по приложению \"Нарисуй меня\"")
                .contact("Марат Амиров")
                .version("1.0")
                .build();
    }

    @Bean
    public EmbeddedServletContainerFactory servletContainer() {
        TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory();
        tomcat.addAdditionalTomcatConnectors(createSslConnector());
        return tomcat;
    }

    private Connector createSslConnector() {
        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
        Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
        try {
            File keystore = new ClassPathResource("certificate.pfx").getFile();
            File truststore = new ClassPathResource("certificate.pfx").getFile();
            connector.setScheme("https");
            connector.setSecure(true);
            connector.setPort(443);
            protocol.setKeystoreType("PKCS12");
            protocol.setSSLEnabled(true);
            protocol.setKeystoreFile(keystore.getAbsolutePath());
            protocol.setKeystorePass("");
            protocol.setTruststoreFile(truststore.getAbsolutePath());
            protocol.setTruststorePass("");
            return connector;
        }
        catch (IOException ex) {
            throw new IllegalStateException("can't access keystore: [" + "keystore"
                    + "] or truststore: [" + "keystore" + "]", ex);
        }
    }


}
