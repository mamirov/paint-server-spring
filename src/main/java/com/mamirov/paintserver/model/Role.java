package com.mamirov.paintserver.model;

public enum Role {
    ADMIN,
    CLIENT,
    PRO_CLIENT
}
