package com.mamirov.paintserver.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "user", schema = "drawme")
public class User {

    @Id
    @GeneratedValue
    @Column(name = "user_id")
    @JsonIgnore
    private Integer id;

    @JsonProperty("id")
    @Column(name = "vk_id")
    @ApiModelProperty(notes = "ID вконтакте", required = true)
    private String vkId;

    @JsonProperty("domain")
    @Column(name = "login")
    @ApiModelProperty(notes = "Доменное имя вконтакке", required = true)
    private String login;

    @JsonProperty("first_name")
    @Column(name = "first_name")
    @ApiModelProperty(notes = "Фамилия", required = true)
    private String firstName;

    @JsonProperty("last_name")
    @Column(name = "last_name")
    @ApiModelProperty(notes = "Имя", required = true)
    private String lastName;

    @JsonProperty("energy")
    @Column(name = "energy")
    @ApiModelProperty(notes = "Энергия", required = true)
    private Integer energy = 30;

    @JsonProperty("exp")
    @Column(name = "experience")
    @ApiModelProperty(notes = "Опыт", required = true)
    private Long experience = 0L;

    @Column(name = "gold")
    @ApiModelProperty(notes = "Золото", required = true)
    private Long gold = 0L;

    @JsonIgnore
    @Column(name = "energy_update")
    private Date energyUpdate;

    @JsonIgnore
    @Column(name = "user_role")
    @Enumerated(EnumType.STRING)
    private Role role;

    @JsonIgnore
    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Picture> drawPictures = new ArrayList<Picture>();

    @OneToMany(orphanRemoval = true, mappedBy = "targetUser", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private  List<UserNotification> notifications = new ArrayList<UserNotification>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getVkId() {
        return vkId;
    }

    public void setVkId(String vkId) {
        this.vkId = vkId;
    }

    public List<Picture> getDrawPictures() {
        return drawPictures;
    }

    public void setDrawPictures(List<Picture> drawPictures) {
        this.drawPictures = drawPictures;
    }

    public Integer getEnergy() {
        return energy;
    }

    public void setEnergy(Integer energy) {
        this.energy = energy;
    }

    public Date getEnergyUpdate() {
        return energyUpdate;
    }

    public void setEnergyUpdate(Date energyUpdate) {
        this.energyUpdate = energyUpdate;
    }

    public Long getExperience() {
        return experience;
    }

    public void setExperience(Long experience) {
        this.experience = experience;
    }

    public Long getGold() {
        return gold;
    }

    public void setGold(Long gold) {
        this.gold = gold;
    }

    public List<UserNotification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<UserNotification> notifications) {
        this.notifications = notifications;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", vkId='" + vkId + '\'' +
                ", login='" + login + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", energy=" + energy +
                ", experience=" + experience +
                ", gold=" + gold +
                ", energyUpdate=" + energyUpdate +
                ", role=" + role +
                ", drawPictures=" + drawPictures +
                ", notifications=" + notifications +
                '}';
    }
}
