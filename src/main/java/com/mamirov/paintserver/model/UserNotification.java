package com.mamirov.paintserver.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "user_notification", schema = "drawme")
public class UserNotification {

    @Id
    @GeneratedValue
    @Column(name = "notification_id")
    private Long id;

    @Column(name =  "status")
    private boolean read = false;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private UserNotificationType type;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User targetUser;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public User getTargetUser() {
        return targetUser;
    }

    public void setTargetUser(User targetUser) {
        this.targetUser = targetUser;
    }

    public UserNotificationType getType() {
        return type;
    }

    public void setType(UserNotificationType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "UserNotification{" +
                "id=" + id +
                ", read=" + read +
                ", type=" + type +
                ", targetUser=" + targetUser.getId() +
                '}';
    }
}
