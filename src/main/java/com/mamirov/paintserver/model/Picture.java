package com.mamirov.paintserver.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

@Entity
@Table(name = "picture", schema = "drawme")
public class Picture {

    @Id
    @GeneratedValue
    @Column(name = "id")
    @ApiModelProperty(notes = "ID", required = true)
    private Integer id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "author_id", referencedColumnName = "user_id")
    private User author;

    @ManyToOne
    @ApiModelProperty(notes = "Пользователь, которого нарисовали", required = true)
    @JoinColumn(name = "target_user_id", referencedColumnName = "user_id")
    private User targetUser;

    @Column(name = "link")
    @ApiModelProperty(notes = "Ссылка на рисунок", required = true)
    private String link;

    @Column
    @ApiModelProperty(notes = "Рейтинг", required = true)
    private Long rating;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public User getTargetUser() {
        return targetUser;
    }

    public void setTargetUser(User targetUser) {
        this.targetUser = targetUser;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Long getRating() {
        return rating;
    }

    public void setRating(Long rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Picture{" +
                "id=" + id +
                ", author=" + author +
                ", targetUser=" + targetUser +
                ", link='" + link + '\'' +
                ", rating=" + rating +
                '}';
    }
}
