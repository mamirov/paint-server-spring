package com.mamirov.paintserver.utils;

import com.mamirov.paintserver.model.User;
import org.springframework.security.core.context.SecurityContextHolder;

public class PaintContext {

    public static User getCurrentUser() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
