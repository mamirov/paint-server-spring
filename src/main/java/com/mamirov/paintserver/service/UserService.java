package com.mamirov.paintserver.service;

import com.mamirov.paintserver.model.User;

import java.util.List;

public interface UserService {

    User findById(Integer id);

    User findByLogin(String login);

    User findByVkId(String vkId);

    List<User> findAll();

    List<User> findByEnergyLessThan(Integer energy);

    List<User> findByVkIdNot(String vkId);

    User save(User user);

    List<User> save(List<User> users);
}
