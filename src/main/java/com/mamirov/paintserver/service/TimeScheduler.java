package com.mamirov.paintserver.service;

import com.mamirov.paintserver.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class TimeScheduler {

    @Autowired
    private UserService userService;

    @Scheduled(fixedRate = 15000)
    public void updateUsersEnergy() {
        List<User> users = userService.findByEnergyLessThan(30);
        long nowDate = new Date().getTime();
        for(User user : users) {
            if(user.getEnergyUpdate().getTime() < nowDate) {
                user.setEnergy(user.getEnergy() + 1);
                if(user.getEnergy() < 30) {
                    user.setEnergyUpdate(new Date(nowDate + 1000*60*5));
                }
            }
        }
        if(!users.isEmpty()) {
            userService.save(users);
        }
    }
}
