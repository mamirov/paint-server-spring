package com.mamirov.paintserver.service;

import com.mamirov.paintserver.dao.UserRepository;
import com.mamirov.paintserver.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User findById(Integer id) {
        return userRepository.findOne(id);
    }

    @Override
    public User findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    public User findByVkId(String vkId) {
        return userRepository.findByVkId(vkId);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public List<User> findByEnergyLessThan(Integer energy) {
        return userRepository.findByEnergyLessThan(energy);
    }

    @Override
    public List<User> findByVkIdNot(String vkId) {
        return userRepository.findByVkIdNot(vkId);
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public List<User> save(List<User> users) {
        return userRepository.save(users);
    }


}
