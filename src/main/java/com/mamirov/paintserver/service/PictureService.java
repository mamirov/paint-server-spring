package com.mamirov.paintserver.service;

import com.mamirov.paintserver.model.Picture;
import com.mamirov.paintserver.model.User;

import java.util.List;

public interface PictureService {

    Picture findById(Integer id);

    List<Picture> findByAuthor(User user);

    List<Picture> findByTargetUser(User targetUser);

    Picture save(Picture picture);
}
