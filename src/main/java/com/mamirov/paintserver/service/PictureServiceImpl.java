package com.mamirov.paintserver.service;

import com.mamirov.paintserver.dao.PictureRepository;
import com.mamirov.paintserver.model.Picture;
import com.mamirov.paintserver.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PictureServiceImpl implements PictureService {

    @Autowired
    private PictureRepository pictureRepository;

    @Override
    public Picture findById(Integer id) {
        return pictureRepository.findOne(id);
    }

    @Override
    public List<Picture> findByAuthor(User user) {
        return pictureRepository.findByAuthor(user);
    }

    @Override
    public List<Picture> findByTargetUser(User targetUser) {
        return pictureRepository.findByTargetUser(targetUser);
    }

    @Override
    public Picture save(Picture picture) {
        return pictureRepository.save(picture);
    }
}
